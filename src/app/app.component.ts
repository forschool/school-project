import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'school-project';
  lang:any;
  constructor(private translateService:TranslateService){
    this.translateService.setDefaultLang('en');
    this.translateService.use(localStorage.getItem('lang')|| "en");
  }
  ngOnInit(){
    this.lang=localStorage.getItem("lang") || "en";
  }
  changeLang(lang){
    localStorage.setItem("lang",lang);
    window.location.reload();
  }
}
