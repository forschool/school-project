import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './view/auth/login/login.component';
import { BrandCreateComponent } from './view/brand/brand-create/brand-create.component';
import { BrandComponent } from './view/brand/brand/brand.component';
import { DashboardComponent } from './view/dashboard/dashboard.component';
import { ErrorComponent } from './view/error/error.component';

const routes: Routes = [
  {path:"",redirectTo:"dashboard",pathMatch:"full"},
  {path:"login",component:LoginComponent},
  {path:"dashboard",component:DashboardComponent},
  {path:"brand",component:BrandComponent},
  {path:"brand-create",component:BrandCreateComponent},
  {path:"**",component:ErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
