import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './view/dashboard/dashboard.component';
import { ErrorComponent } from './view/error/error.component';
import { BrandComponent } from './view/brand/brand/brand.component';
import { BrandCreateComponent } from './view/brand/brand-create/brand-create.component';
import { BrandEditComponent } from './view/brand/brand-edit/brand-edit.component';
import { LoginComponent } from './view/auth/login/login.component';
import { LanguageInterceptor } from './helper/language.interceptor';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ErrorComponent,
    BrandComponent,
    BrandCreateComponent,
    BrandEditComponent,
    LoginComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
  ],
  providers: [
    {
      provide:HTTP_INTERCEPTORS,
      useClass:LanguageInterceptor,
      multi:true
    },
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
